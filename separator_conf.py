from dataclasses import dataclass

from gpiozero import LED, Button


@dataclass
class Separator1Pins:
    # inputs
    start: Button = Button(11)
    stop: Button = Button(0)
    temp_relay: Button = Button(5)
    is_work: Button = Button(6)

    # outputs
    work_signal: LED = LED(15)
