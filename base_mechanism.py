from abc import abstractmethod, ABC

from gpiozero import Button, LED


class Mechanism(ABC):
    _work_signal: LED
    _temp_relay: Button
    _is_work: Button

    _mechanism_number: int

    def __init__(self, work_signal: LED, temp_relay: Button, is_work: Button, mechanism_number: int):
        self._work_signal = work_signal
        self._temp_relay = temp_relay
        self._is_work = is_work
        self._mechanism_number = mechanism_number

    def start(self):
        print('start')
        self._work_signal.on()

    def stop(self):
        print('stop')
        self._work_signal.off()

    @abstractmethod
    async def check_if_work(self):
        pass

    @abstractmethod
    async def get_status(self):
        pass
