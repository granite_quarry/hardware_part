import asyncio

from gpiozero import LED, Button
from base_mechanism import Mechanism


class Conveyor(Mechanism):
    _rope: Button
    _descent: Button
    _fence: Button

    _speed_counter: int = 0
    _speed_threshold: int = 4
    _is_rotate: bool = True

    def __init__(self, work_signal: LED, temp_relay: Button, is_work: Button, rope: Button, descent: Button,
                 fence: Button, conveyor_number: int):
        super().__init__(work_signal, temp_relay, is_work, conveyor_number)
        self._rope = rope
        self._descent = descent
        self._fence = fence

    def stop(self):
        super().stop()
        self._is_rotate = True

    @property
    def speed_threshold(self):
        return self._speed_threshold

    @speed_threshold.setter
    def speed_threshold(self, speed_threshold):
        self._speed_threshold = speed_threshold

    async def reset_counter(self):
        await asyncio.sleep(1)
        self.__check_is_rotate()
        self._speed_counter = 0

    def raise_counter(self):
        self._speed_counter += 1

    async def check_if_work(self):
        await asyncio.sleep(0.5)
        if (self._temp_relay.is_pressed
                or not self._is_rotate
                or not self._is_work.is_pressed
                or not self._rope.is_pressed
                or not self._descent.is_pressed
                or not self._fence.is_pressed):
            self._work_signal.off()

    async def get_status(self):
        status = f'Conveyor {self._mechanism_number}\n' \
                 f'rotation = {self._is_rotate}, work => {self._is_work.is_pressed}, ' \
                 f'rope => {self._rope.is_pressed}, descent => {self._descent.is_pressed}, ' \
                 f'fence => {self._fence.is_pressed}, temperature relay => {self._temp_relay.is_pressed}'
        print(status)
        await asyncio.sleep(1)

    def __check_is_rotate(self):
        if self._speed_counter < self._speed_threshold:
            self._is_rotate = False
