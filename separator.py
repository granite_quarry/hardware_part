import asyncio

from gpiozero import LED, Button

from base_mechanism import Mechanism


class Separator(Mechanism):
    def __init__(self, work_signal: LED, temp_relay: Button, is_work: Button, separator_number: int):
        super().__init__(work_signal, temp_relay, is_work, separator_number)

    async def check_if_work(self):
        await asyncio.sleep(0.5)
        if self._temp_relay.is_pressed or not self._is_work.is_pressed:
            self._work_signal.off()

    async def get_status(self):
        status = f'Separator {self._mechanism_number}\n' \
                 f'work => {self._is_work.is_pressed}, temperature relay => {self._temp_relay.is_pressed}'

        print(status)
        await asyncio.sleep(1)
