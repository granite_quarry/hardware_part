import asyncio

from conveyor import Conveyor
from conveyor_conf import Conveyor1Pins
from separator import Separator
from separator_conf import Separator1Pins


async def create_tasks():
    reset_speed_counter = asyncio.create_task(conv_1.reset_counter())
    conveyor_protection = asyncio.create_task(conv_1.check_if_work())
    conveyor_status = asyncio.create_task(conv_1.get_status())
    separator_protection = asyncio.create_task(sep_1.check_if_work())
    separator_status = asyncio.create_task(sep_1.get_status())

    await reset_speed_counter
    await conveyor_protection
    await conveyor_status
    await separator_protection
    await separator_status


if __name__ == '__main__':
    conv_1_pins = Conveyor1Pins()
    conv_1 = Conveyor(work_signal=conv_1_pins.work_signal, temp_relay=conv_1_pins.temp_relay,
                      is_work=conv_1_pins.is_work, rope=conv_1_pins.rope, descent=conv_1_pins.descent,
                      fence=conv_1_pins.fence, conveyor_number=1)

    conv_1_pins.start.when_pressed = conv_1.start
    conv_1_pins.stop.when_pressed = conv_1.stop
    conv_1_pins.speed_counter.when_pressed = conv_1.raise_counter

    sep_1_pins = Separator1Pins()
    sep_1 = Separator(work_signal=sep_1_pins.work_signal, temp_relay=sep_1_pins.temp_relay, is_work=sep_1_pins.is_work,
                      separator_number=1)

    sep_1_pins.start.when_pressed = sep_1.start
    sep_1_pins.stop.when_pressed = sep_1.stop

    while True:
        asyncio.run(create_tasks())
