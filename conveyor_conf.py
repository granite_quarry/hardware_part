from dataclasses import dataclass

from gpiozero import LED, Button


@dataclass
class Conveyor1Pins:
    # inputs
    start: Button = Button(2)
    stop: Button = Button(3)
    speed_counter: Button = Button(4)
    temp_relay: Button = Button(17)
    is_work: Button = Button(27)

    rope: Button = Button(22)
    descent: Button = Button(10)
    fence: Button = Button(9)

    # outputs
    work_signal: LED = LED(14)
